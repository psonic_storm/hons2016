# downloader.py
# Description : downloads an image from the network camera every x seconds, detects motion and saves image if motion was detected.
# Author : Paul Sebeikin
# Date-created : 14 August 2016
# Date-modified : 16 September 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse
    from datetime import datetime
    from threading import Timer
    import numpy as np
    import imutils
    import os
    import math
    import cv2
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, elapsed, total_duration, recording_left = 0,0,0,0,0
writer, dest, start = None, None, None
(resWidth,resHeight) = (None, None)

def image_download(url):
    # url = 'http://192.168.1.4/image/jpeg.cgi'
    path = urllib.urlopen(url)
    image = np.asarray(bytearray(path.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image

def init_recorder(dest,w,h):
    res = '%ix%i' % (w,h)
    outpath = dest + 'FD_still_frame_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G') #('W', 'M', 'V', '2')
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer

def main():
    global counter, counter_processed, elapsed, total_duration, recording_left, writer, resWidth, resHeight, dest, start

    # Parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument( '-u', "--url", help="path to the url")
    ap.add_argument( '-o', "--output", help="output directory")
    ap.add_argument( '-d', "--duration", type=int, default=120, help="time in seconds to run experiment")
    ap.add_argument( '-a', "--min-area", type=int, default=500, help="minimum area size for finding and tracking contours")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_argument( "--heirarchy", type=bool, default=False, help='use heirarchy to filter contours')
    ap.add_argument( '-t', "--thresh", type=int, default=120, help="threshold value to use")
    ap.add_argument( '--view', type=bool, default=False, help='set to true if a gui is required')
    args = vars(ap.parse_args())

    url = args["url"]
    (resWidth,resHeight) = args["resolution"]  
    dest = args["output"]
    time = args["duration"]
    min_area = args["min_area"]
    threshold_val = args["thresh"]
    useHeirarchy = args["heirarchy"]
    view = args["view"]
    
    firstFrame = None   
    
    # motion detection preamble
    start = datetime.now()
    Timer(time, exitfunction).start()    
    start_frame_time = datetime.now()
    print "[INFO] : Downloader is running..."
    while True:
        frame = image_download(url)
        frame = imutils.resize(frame, width=resWidth)
        
        if frame is None:
            print '[ERROR] : Frame not received from the camera'
            break

        if counter < 2: # skip first two frames to allow for camera warm up   
            counter += 1
            continue
        
        text = "Idle"

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # greyscale the image
        preprocessing.gaussianblur(gray)

        if firstFrame is None:
            firstFrame = gray
            continue

        process_start = datetime.now()  
        frameDelta = cv2.absdiff(firstFrame, gray)
        thresh = cv2.threshold(frameDelta, threshold_val, 255, cv2.THRESH_BINARY)[1] # threshold(src, threshold, maxVal, )
        thresh = cv2.dilate(thresh, None, iterations=2)
        (_, cnts, hierarchy) = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)                           

        largest_contour = (None, None)
        for i in range(len(cnts)):        
            area = cv2.contourArea(cnts[i])
            if area < min_area:
                continue
            if useHeirarchy:                
                if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
                    continue
            elif largest_contour is None:
                largest_contour = (cnts[i], area)
            elif area > largest_contour[1]:
                largest_contour = (cnts[i], area)
            
        if (largest_contour[0] is not None):
            (x, y, w, h,) = cv2.boundingRect(largest_contour[0])
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 9), 2)
            text = "Motion Detected"
            recording_left = 10000

        if (recording_left > 0):
            cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if writer is None:
                writer = init_recorder(dest, resWidth, resHeight)
            else:
                writer.write(frame) #Write the frame

                process_end = datetime.now()
                duration = process_end - process_start
                seconds = duration.total_seconds()
                total_duration += seconds
                counter_processed += 1 # keeps track of number of frames recorded

        counter += 1 # keeps track of number of frames processed
        end = datetime.now()
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            # print recording_left
            start_frame_time = datetime.now()

        if view:
            cv2.imshow("First Frame", firstFrame)
            cv2.imshow("Security Feed", frame)
            cv2.imshow("Frame Delta", frameDelta)
            cv2.imshow("Thresh", thresh)
        
        key = cv2.waitKey(1) & 0xFF    

        if key == ord("q"):            
            break
    if writer is not None:
        writer.release()
    exitfunction()

def exitfunction():
    # Fetch global variables
    global counter, total_duration, counter_processed
    global resWidth,resHeight, writer

    end = datetime.now()
    elapsed = (end - start).total_seconds()

    f = open(dest + 'results_frame_diff.txt', 'a')
    f.write('[REPORT] : Frame differencing - motion detection operation ran for %i seconds\n' % elapsed)
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (resWidth,resHeight))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter, counter_processed))

    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (resWidth,resHeight)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print '[REPORT] : FPS : %f' % ((counter / elapsed))
    print '[DONE] : Operation completed at %s' % datetime.now()
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.close()
    if writer is not None:
        writer.release()
    os._exit(0) # exit the program


if __name__ == "__main__":
    main()
