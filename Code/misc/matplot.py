import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('images.png', 0)
plt.imshow(img, cmap='afmhot', interpolation='bicubic')
plt.xticks([]), plt.yticks([])
plt.show()
plt.waitKey(0)
plt.destroyAllWindows()

