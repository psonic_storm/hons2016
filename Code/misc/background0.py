import cv2
import numpy as np

path = 'http://admin:ps891228@192.168.1.4/video.cgi?resolution=640x520'
cap = cv2.VideoCapture(path)

fgbg = cv2.createBackgroundSubtractorKNN()

while(True):
    ret, frame = cap.read()

    fgmask = fgbg.apply(frame)

    cv2.imshow('video_feed', fgmask)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
