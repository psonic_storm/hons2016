import cv2
import argparse
import time
import numpy as np
import imutils
from threading import Timer
from time import sleep, time
import os

dest = None
output = None
resWidth, resHeight = None,None

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( "-p", "--path", help="path to the video file")
    ap.add_argument( "-c", "--camera", type=int, help="locally connected camera ID")
    ap.add_argument( "-o", "--dest", help="output path for the video file")
    ap.add_argument( "-d", "--duration", type=int, default=300, help="length of recording")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_arguent('--view', type=bool, help='display video feed toggle')
    args = vars(ap.parse_args())

    global dest, resWidth, resHeight, output

    dest = args["dest"]
    path = args["path"]   
    (resWidth,resHeight) = args["resolution"]
    time = args["duration"]
    camID = args["camera"]
    view = args["view"]

    if path is None and camID is None:
        print '[ERROR] : At least one streaming device must be specified.'
        os._exit(0)
    if path is None:
        camera = cv2.VideoCapture(camID)
        print('[INFO] : Streaming from built-in camera')        
    else:        
        camera = cv2.VideoCapture(path)   
        print('[INFO] : Streaming from path : %s' % path) 


    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')

    # print('[INFO]: Preparing to write to %s at %i fps using %s' % ( args["path"], args["fps"], args["codec"] ))

    print('[INFO] : Warming up the camera...')
    camera.set(3, resWidth) # 3 = CV_CAP_PROP_FRAME_WIDTH
    camera.set(4, resHeight) # 4 = CV_CAP_PROP_FRAME_HEIGHT
    camera.set(5, 20) # 5 = CV_CAP_PROP_FPS
    size = (resWidth, resHeight)
    counter = 0    
    sleep(3)

    print '[INFO] : Streaming...'
    Timer(time, exitfunction).start() 

    while(True):
        ret, frame = camera.read()
        if ret==True:
            if output is None:
                (h, w) = frame.shape[:2]
                output = cv2.VideoWriter(dest, codec, 20, (resWidth,resHeight), True)
                print output
            output.write(frame)
            if view:
	    	cv2.imshow('video_feed', frame)
            if cv2.waitKey(1) & 0xFF == ord('c'):
                path = args["path"] + "image" + str(counter) + ".png"
                print('[INFO] : snapping image to %s' % path)
                cv2.imwrite(path, frame)
                counter += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            print '[ERROR] : Cannot grab frame'
            break
    camera.release()
    cv2.destroyAllWindows()
    os._exit(0)

def exitfunction():
    global resWidth, resHeight, output
    output.release()
    print "[DONE] Video saved in %ix%i at %i fps" % (resWidth, resHeight, 20)
    os._exit(0) # exit the program

if __name__ == "__main__":
    main()
