# violajones.py
# Description :  Detecting the face and eyes using haarcascade modules of openCV
# By : Paul Sebeikin
# Date-created : 11 August 2016 
# Date-modified : 11 August 2016
# Source : http://www.docs.opencv.org/master/d7/d8b/tutorial_py_face_detection.html#gsc.tab=0

import numpy as np
import cv2, argparse

face_cascade = cv2.CascadeClassifier( '../xml/haarcascade_frontalface_default.xml' )
eye_cascade = cv2.CascadeClassifier( '../xml/haarcascade_eye.xml' )

ap = argparse.ArgumentParser()
ap.add_argument( "-v", "--video", help="path to the video file" )  
args = vars( ap.parse_args() )

if args["video"] is None:
	cap = cv2.VideoCapture(0) # use default camera
else:
	cap = cv2.VideoCapture(args["video"]) # use camera specified in args

while True:
	( grabbed, frame ) = cap.read()
	if not grabbed:
		break

	gray = cv2.cvtColor( frame, cv2.COLOR_RGB2GRAY )
	faces = face_cascade.detectMultiScale( gray, 1.3, 5, minSize=(30,30))
	for ( x, y, w, h ) in faces:
		cv2.rectangle( frame, (x, y), (x+w, y+h), (255, 0, 0), 2 )
		roi_gray = gray[ y:y+h, x:x+w ]
		roi_color = frame[ y:y+h, x:x+w ]
		eyes = eye_cascade.detectMultiScale( roi_gray )
		for ( ex, ey, ew, eh ) in eyes:
			cv2.rectangle( roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
	cv2.imshow("video", frame)

	key = cv2.waitKey( 1 ) & 0xFF
	if key == ord( "q" ):
		break

cap.release()
cv2.destroyAllWindows()    