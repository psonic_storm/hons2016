import os, sys, cv2

cv2.namedWindow('images', cv2.WINDOW_NORMAL)
img = cv2.imread('/home/pauls/Documents/Dev/images.png', -1)	# reads in image; 0 - greyscale; 1 - color; -1 - unchanged
cv2.imshow('images',img)
cv2.waitKey(0) 													# waits for a key press before closing the image window
cv2.destroyAllWindows()											# kills all open image windows upon key press
print("Save image? y/n")
res = input()													# 100 - max number of character to read in; flag error if more
print(res)
if str(res) == 'y' : 
	print('Saving image...')
	cv2.imwrite('image_grey.png', img)
	print('Image saved.')
else : print('Image not saved.')