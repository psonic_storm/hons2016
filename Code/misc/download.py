from PIL import Image
import sched, time
import urllib
import io

counter = 0

def image_download():
    global counter
    print('Getting image...')
    url = 'http://192.168.1.4/image/jpeg.cgi'
    path = io.BytesIO(urllib.urlopen(url).read())
    image = Image.open(path)  # get image
    print('Saving image...')
    out = '../output/image%i.jpeg' % counter
    image.save(out)   # save image
    print('Image saved')

def main():
    global counter
    start_time = time.time()
    while True:
        # s = sched.scheduler(time.time, time.sleep)
        # s.enter(10,1, image_download, argument=())
        #s.run()
        image_download()
        counter +=1
        time.sleep(5) # sleep for ten seconds

if __name__ == "__main__":
    main()
