from datetime import datetime
from time import time, sleep

elapsed,x = 0,0

while x < 5:
	start = datetime.now()
	sleep(5)
	end = datetime.now()
	elapsed += (end - start).total_seconds()
	x += 1

# y = elapsed / x
# print y
# reporting
print '[REPORT] : Operation ran for %i seconds' % elapsed
# print '[REPORT] : %i frames processed' % counter
print '[REPORT] : Average frame processing time : %f seconds' % round((elapsed / x),2)

