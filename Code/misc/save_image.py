import cv2, os, argparse

camera = None
counter = 0

def get_image():
    global camera

    return im

def main():
    global camera, counter
    ap = argparse.ArgumentParser()
    ap.add_argument( "-v", "--video", help="path to the video camera")
    ap.add_argument( "-p", "--path", help="output path for the image file")
    args = vars(ap.parse_args())

    if args.get("video", None) is None:
        camera = cv2.VideoCapture(0)
        print('[INFO] : Streaming from built-in camera')
    else:
        camera = cv2.VideoCapture(args["video"])
        print('[INFO] : Streaming from network path : %s' % args["video"])

    print('[INFO] : Warming up the camera...')


    status = '[INFO] : waiting to take image...'
    print(status)

    while True:
        retval, frame = camera.read()
        if retval == True:
            cv2.imshow('Camera', frame)
            if cv2.waitKey(1) & 0xFF == ord('c'):
                path = args["path"] + "image" + str(counter) + ".png"
                print('[INFO] : snapping image to %s' % path)
                cv2.imwrite(path, frame)
                counter += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    # clean up
    camera.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
