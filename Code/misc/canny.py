import argparse
import imutils
import cv2
import time

ap = argparse.ArgumentParser()
ap.add_argument( "-v", "--video", help="path to the video file")
ap.add_argument("-a", "--min-area", type=int, default=100, help="minimum area size")
args = vars(ap.parse_args())

if args.get("video", None) is None:
    camera = cv2.VideoCapture(0)
    time.sleep(0.25)
else:
    camera = cv2.VideoCapture(args["video"])

while True:
    (grabbed, frame) = camera.read()
    if not grabbed:
        break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #frame = imutils.resize(frame, width=800)
    edgeMap = imutils.auto_canny(gray)
    edgeMap = imutils.resize(edgeMap, width=800)
    #cv2.imshow("Original", frame)
    cv2.imshow("Automatic Edge Map", edgeMap)

    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break
q
camera.release()
cv2.destroyAllWindows()
