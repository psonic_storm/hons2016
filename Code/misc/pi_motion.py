try:
    print "[INFO] Importing Libraries..."
    import argparse
    from datetime import datetime
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    from threading import Timer
    import numpy as np
    import imutils
    import os
    import io
    import math
    import cv2 as cv
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

# global variables
counter, counter_processed, total_duration = 0,0,0
(w,h) = None,None
start = None

def init_recorder(dest):
    codec = cv.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')##('I', '4', '2', '0')#
    writer = cv.VideoWriter(dest + datetime.now().strftime("%b-%d-%Y")+".avi", codec, 20, (640,480), 1)
    return writer

def main():
    # Fetch global variables
    global counter, total_duration, counter_processed
    global w,h, start

    ap = argparse.ArgumentParser()
    ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
    ap.add_argument("-d", "--dest", help="path to the output file")
    ap.add_argument("-t", "--time", type=int, default=120, help="time in seconds to run experiment")
    args = vars(ap.parse_args())

    # Local Variables
    dest = args["dest"]
    time = args["time"]
    min_area = args["min_area"]
    firstFrame = None
    timeStart = datetime.now()

    recording_left = 0
    writer = None

    camera = PiCamera()
    camera.resolution = (640,480)
    camera.framerate = 20
    rawCapture = PiRGBArray(camera, size=(640,480))
  
    print('[INFO] : Warming up the camera...')
    sleep(2.0)
    
    print "[INFO] : PiCamera motion detector is running..."

    start = datetime.now()
    Timer(time, exitfunction).start()    

    start_frame_time = datetime.now()
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        
        image = frame.array
        image = imutils.rotate(image, 180)

        text = "Idle"        
        (h, w) = image.shape[:2]
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY) # greyscale the image
        preprocessing.gaussianblur(gray)

        if firstFrame is None:
            firstFrame = gray
            rawCapture.truncate(0)
            continue

        frameDelta = cv.absdiff(firstFrame, gray)
        thresh = cv.threshold(frameDelta, 150, 255, cv.THRESH_BINARY)[1] # threshold(src, threshold, maxVal, )
        thresh = cv.dilate(thresh, None, iterations=2)
        (_, cnts, hierarchy) = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
       
        process_start = datetime.now()
        rectangles = 0          

        # draw only the valid contours  
        for i in range(len(cnts)):
            # if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
            #     # print 'hierarchy fail'
            #     continue
            if cv.contourArea(cnts[i]) < min_area:
                # print 'contour area fail'
                continue
            (x, y, w, h,) = cv.boundingRect(cnts[i])
            cv.rectangle(image, (x, y), (x + w, y + h), (0, 255, 9), 2)
            text = "Motion Detected"
            rectangles += 1

        if (rectangles > 0):
            recording_left = 10000          

        if (rectangles > 0 or recording_left > 0):
            cv.putText(image, "Status: {}".format(text), (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv.putText(image, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, image.shape[0] - 10), cv.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if writer is None:
                writer = init_recorder(dest)
            else:                
                writer.write(image) #Write the frame

            process_end = datetime.now()
            duration = process_end - process_start
            seconds = duration.total_seconds()
            total_duration += seconds
            counter_processed += 1 # keeps track of number of frames recorded

        counter += 1 # keeps track of number of frames processed
        
	    # print elapsed, time
        rawCapture.truncate(0)
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            # print recording_left
            start_frame_time = datetime.now()
        
        # cv.imshow("Security Feed", frame)
        # cv.imshow("Frame Delta", frameDelta)
        # cv.imshow("Thresh", thresh)
        
        key = cv.waitKey(1) & 0xFF    

        if key == ord("q"):            
            break

    camera.release()
    cv.destroyAllWindows()

def exitfunction():
    # Fetch global variables
    global counter, total_duration, counter_processed
    global w,h

    end = datetime.now()
    elapsed = (end - start).total_seconds()


    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (w,h)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        print '[REPORT] : No frames recorded.'
    print '[REPORT] : FPS : %f' % ((counter / elapsed))    
    os._exit(0) # exit the program

if __name__ == "__main__":
    main()

