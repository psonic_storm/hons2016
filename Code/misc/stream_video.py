import cv2
import argparse
import time
import numpy as np
import imutils

output = None

ap = argparse.ArgumentParser()
ap.add_argument( "-v", "--video", help="path to the video file")
args = vars(ap.parse_args())

if args.get("video", None) is None:
    camera = cv2.VideoCapture(1)
    print('[INFO] : Streaming from built-in camera')
else:
    camera = cv2.VideoCapture(args["video"])
    camera.set(3, 640) # 3 = CV_CAP_PROP_FRAME_WIDTH
    camera.set(4, 480) # 4 = CV_CAP_PROP_FRAME_HEIGHT
    print('[INFO] : Streaming from network path : %s' % args["video"])

print('[INFO] : Warming up the camera...')

while True:
    ret, frame = camera.read()
    '''if not ret:
        print('[ERROR] : Stream error.')
        break'''
    #(h, w) = frame.shape[:2]
    #print('Height=%i, Width=%i' % (h, w) )
    cv2.imshow('Stream', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
	    break

camera.release()
cv2.destroyAllWindows()
