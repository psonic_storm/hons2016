try:
    print "[INFO] Importing Libraries..."
    import argparse
    from datetime import datetime
    from threading import Timer
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    import numpy as np
    import imutils
    import os
    import math
    import cv2
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, total_duration = 0,0,0
(resWidth,resHeight) = (None, None)
start, writer, camera, dest = None, None, None, None

def init_recorder(dest, w,h):
    res = '%ix%i' % (w,h)
    outpath = dest + 'VJ_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')##('I', '4', '2', '0')#
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer

def main():
     # Fetch global variables
    global counter, total_duration, counter_processed
    global resWidth, resHeight, start, camera, dest, writer

    ap = argparse.ArgumentParser()
    ap.add_argument("-o", "--output", help="path to the output file")
    ap.add_argument("-d", "--duration", type=int, default=120, help="time in seconds to run experiment")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_argument( '--view', type=bool, default=False, help='set to true if a gui is required')
    args = vars(ap.parse_args())

    # Local Variables
    dest = args["output"]
    time = args["duration"]
    (resWidth,resHeight) = args["resolution"]  
    timeStart = datetime.now()
    recording_left = 0
    view = args["view"]
    writer = None

    face_cascade = cv2.CascadeClassifier( './xml/haarcascade_frontalface_alt.xml' )
    eye_cascade = cv2.CascadeClassifier( './xml/haarcascade_eye.xml' )

    print('[INFO] : Warming up the Pi camera...')
    camera = PiCamera()
    camera.resolution = (resWidth,resHeight)
    camera.framerate = 20
    rawCapture = PiRGBArray(camera, size=(resWidth,resHeight))
    sleep(2.0)   

    # setup end time and record timing information
    start = datetime.now()
    Timer(time, exitfunction).start()    
    start_frame_time = datetime.now()
    print "[INFO] : PiCamera motion detector is running..."
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        if frame is None:
            print '[ERROR] : Frame not received from the camera'
            break
        frame = frame.array
        frame = imutils.rotate(frame, 180)
        text = "Idle"

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # greyscale the image
        # preprocessing.gaussianblur(gray)
        # tracking variables
        faces_found = 0
        eyes_found = 0

        faces = face_cascade.detectMultiScale( gray, 1.3, 5, minSize=(30,30))
        for ( x, y, w, h ) in faces:
            cv2.rectangle( frame, (x, y), (x+w, y+h), (255, 0, 0), 2 )
            faces_found += 1
            roi_gray = gray[ y:y+h, x:x+w ]
            roi_color = frame[ y:y+h, x:x+w ]
            eyes = eye_cascade.detectMultiScale( roi_gray )

            for ( ex, ey, ew, eh ) in eyes:
                cv2.rectangle( roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
                eyes_found += 1
        process_start = datetime.now()
        rectangles = 0          

        if (faces_found > 0 or eyes_found > 0):
            text = "Motion Detected"
            recording_left = 10000

        if (faces_found > 0 or eyes_found > 0 or recording_left > 0):
            cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if writer is None:
                writer = init_recorder(dest,resWidth,resHeight)
            else:     
                writer.write(frame) #Write the frame

            process_end = datetime.now()
            duration = process_end - process_start
            seconds = duration.total_seconds()
            total_duration += seconds
            counter_processed += 1 # keeps track of number of frames recorded

        counter += 1 # keeps track of number of frames processed
        end = datetime.now()
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            # print recording_left
            start_frame_time = datetime.now()

        if view:
            cv2.imshow("Security Feed", frame)
        
        key = cv2.waitKey(1) & 0xFF    
        rawCapture.truncate(0)
        if key == ord("q"):            
            break
    if writer is not None:
        writer.release()
    exitfunction()

def exitfunction():
    # Fetch global variables
    global counter, total_duration, counter_processed
    global resWidth,resHeight, camera, writer

    # camera.release()
    cv2.destroyAllWindows()

    end = datetime.now()
    elapsed = (end - start).total_seconds()

    f = open(dest + 'results_viola_jones.txt', 'a')
    f.write('[REPORT] : Viola Jones - motion detection operation ran for %i seconds\n' % elapsed)
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (resWidth,resHeight))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter, counter_processed))

    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (resWidth,resHeight)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print counter, elapsed
    print '[REPORT] : FPS : %f' % ((counter / elapsed))
    print '[DONE] : Operation completed at %s' % datetime.now()
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.close()
    if writer is not None:
        writer.release()
    os._exit(0) # exit the program

if __name__ == "__main__":
    main()

