try:
    print "[INFO] Importing Libraries..."
    import argparse    
    from threading import Timer
    from time import time, sleep
    from datetime import datetime
    import numpy as np
    import imutils
    import os
    import cv2
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, total_duration = 0,0,0
camera, writer, start, dest, alg = None, None, None, None, None
(resWidth, resHeight) = (None, None)

def init_recorder(dest, w,h, alg):
    algStr = None
    if alg == 0:
        algStr = 'MOG'
    elif alg == 1:
        algStr = 'MOG2'
    elif alg == 2:
        algStr = 'GMG' 
    res = '%ix%i' % (w,h)
    print '[INFO] : Using Algorithm - %i and res %s' % (alg, res)
    outpath = dest + 'BG_' + algStr + '_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')#('I', '4', '2', '0')###
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer

def init_subtracotr(type):
    subtractor = None
    if type == 0:
        subtractor = cv2.bgsegm.createBackgroundSubtractorMOG()
    elif type == 1:
        subtractor = cv2.createBackgroundSubtractorMOG2()
    elif type == 2:
        subtractor = cv2.bgsegm.createBackgroundSubtractorGMG()
    else:
        print '[ERROR] : Invalid background subtraction algorithm entered : %s' % type
    return subtractor

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument( '-p', "--path", help="path to the video file (or url)")
    ap.add_argument( '-c', "--camera", type=int, help="locally connected camera ID")
    ap.add_argument( "-m", "--area_range", type=int, nargs='*', help="range of area for finding and tracking contours")
    ap.add_argument( '-a', '--algorithm', type=int, help='algorithm to use for background subtraction.  0 : MOG, 1 : MOG2, 2 : GMG')
    ap.add_argument( '-d', "--duration", type=int, default=120, help="time in seconds to run experiment")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_argument( '--heirarchy', type=bool, default=False, help='use heirarchy to filter contours')
    ap.add_argument( '-o', "--output", help="path to the output file")
    ap.add_argument( '--view', type=bool, default=False, help='set to true if a gui is required')
    args = vars(ap.parse_args())

    global total_duration, counter, counter_processed
    global resWidth,resHeight, camera, writer, start, dest, alg

    (resWidth,resHeight) = args["resolution"]
    alg = args["algorithm"]
    path = args["path"]
    camID = args["camera"]
    dest = args["output"]
    areaMin, areaMax = args["area_range"]   
    useHeirarchy = args["heirarchy"]
    time = args["duration"]
    view = args["view"]
    writer = None

    if path is None and camID is None:
        print '[ERROR] : At least one streaming device must be specified.'
        os._exit(0)

    elif path is None:
        camera = cv2.VideoCapture(camID)
        print('[INFO] : Streaming from built-in camera')
    else:
        camera = cv2.VideoCapture(path)
        print('[INFO] : Streaming from network path : %s' % path)

    print('[INFO] : Warming up the camera...')
    camera.set(3, resWidth) # 3 = CV_CAP_PROP_FRAME_WIDTH
    camera.set(4, resHeight) # 4 = CV_CAP_PROP_FRAME_HEIGHT
    sleep(2.0)

    kernel = np.ones((5,5), np.uint8)
    fgbg = init_subtracotr(alg)

    start = datetime.now()
    Timer(time, exitfunction).start() 
    start_frame_time = datetime.now()
    recording_left = 0
    print "[INFO] : Motion detector is running..."
    while(True):
        # initilization of variables
        process_start = datetime.now()
        text = "Idle"         

        # processing
        grabbed, frame = camera.read()
        if not grabbed:
            print '[ERROR] : Could not grab frame'
            break

        if counter < 2: # skip first two frames to allow for camera warm up            
            counter += 1
            continue

        fgmask = fgbg.apply(frame)
        if useHeirarchy:
            (_, cnts, hierarchy) = cv2.findContours(fgmask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        else:
            (_, cnts, hierarchy) = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # draw only the largest contours  
        largest_contour = (None, None)
        for i in range(len(cnts)):        
            area = cv2.contourArea(cnts[i])
            if area < areaMin or area > areaMax: # ignore contours smaller than the minimum specified area
                continue
            if useHeirarchy:                
                if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
                    continue
            elif largest_contour is None:
                largest_contour = (cnts[i], area)
            elif area > largest_contour[1]:
                largest_contour = (cnts[i], area)
            
        if (largest_contour[0] is not None):
            (x, y, w, h,) = cv2.boundingRect(largest_contour[0])
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 9), 2)
            text = "Motion Detected"
            recording_left = 10000

        if (recording_left > 0):
            cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if writer is None:
                writer = init_recorder(dest, resWidth, resHeight, alg)
            else:
                writer.write(frame) #Write the frame

                process_end = datetime.now()
                duration = process_end - process_start
                seconds = duration.total_seconds()
                total_duration += seconds
                counter_processed += 1 # keeps track of number of frames recorded

        counter += 1 # keeps track of number of frames processed
        end = datetime.now()
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            start_frame_time = datetime.now()

        if view:
            cv2.imshow('video_feed', frame)
            cv2.imshow('fgmask', fgmask)
        if cv2.waitKey(1) & 0xFF == ord('q'):            
            break

    if writer is not None:
        writer.release()
    exitfunction()

def exitfunction():

    camera.release()
    cv2.destroyAllWindows()

    end = datetime.now()
    elapsed = (end - start).total_seconds()

    f = open(dest + 'results_background_subtraction.txt', 'a')
    f.write('[REPORT] : Background subtraction (Algorithm: %i) - motion detection operation ran for %i seconds\n' % (alg, elapsed))
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (resWidth,resHeight))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter_processed, counter))

    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (resWidth,resHeight)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    print counter, elapsed
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print '[REPORT] : FPS : %f' % ((counter / elapsed))
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.write('[DONE] : Operation completed at %s\n' % datetime.now()) 
    f.close()
    if writer is not None:
        writer.release()
    os._exit(0) # exit the program

if __name__ == '__main__':
    main()