try:
    print "[INFO] Importing Libraries..."
    import argparse
    from datetime import datetime
    from threading import Timer
    import numpy as np
    import imutils
    from imutils.object_detection import non_max_suppression
    import os
    import math
    import cv2
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, total_duration = 0,0,0
(resWidth,resHeight) = (None, None)
start, writer, camera, dest = None, None, None, None
rectsDrawn = 0

def image_download(url):
    # url = 'http://192.168.1.4/image/jpeg.cgi'
    path = urllib.urlopen(url)
    image = np.asarray(bytearray(path.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image


def init_recorder(dest, w,h):
    res = '%ix%i' % (w,h)
    outpath = dest + 'HOG_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')##('I', '4', '2', '0')#
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer


def draw_detections(img, rects, thickness = 1):
    for x,y,w,h in rects:
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0,255,0), thickness)

def main():
     # Fetch global variables
    global counter, total_duration, counter_processed
    global resWidth, resHeight, start, camera, dest, writer, rectsDrawn

    ap = argparse.ArgumentParser()
    ap.add_argument( "-u", "--url", help="path to the url")
    ap.add_argument("-o", "--output", default= None, help="path to the output file")
    ap.add_argument("-d", "--duration", type=int, default=120, help="time in seconds to run experiment")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_argument( '-s', '--scale', type=float, help='scale for the number of layers in the HOG pyramid')
    ap.add_argument( '-w', '--winstride', type=int, nargs='*', help='distance to move sliding window in x and y direction at each inspection')  
    ap.add_argument( '--view', type=bool, default=False, help='set to true if a gui is required')

    args = vars(ap.parse_args())

    # Local Variables
    url = args["url"]    
    dest = args["output"]
    time = args["duration"]
    (resWidth,resHeight) = args["resolution"]  
    (strideX,strideY) = args["winstride"]
    scale = args["scale"]  
    timeStart = datetime.now()
    recording_left = 0
    view = args["view"]
    writer = None

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector( cv2.HOGDescriptor_getDefaultPeopleDetector() )

    # setup end time and record timing information
    start = datetime.now()
    Timer(time, exitfunction).start()    
    start_frame_time = datetime.now()

    print "[INFO] : Body detector downloader is running..."
    while True:
        frame = image_download(url)
        frame = imutils.resize(frame, width=resWidth)
        if frame is None:
            print '[ERROR] : Frame not received from the camera'
            break

        if counter < 2: # skip first two frames to allow for camera warm up            
            counter += 1
            continue
        # frame = imutils.resize(frame, width=min(400, frame.shape[1]))
        resWidth, resHeight = frame.shape[1], frame.shape[0]
        text = "Idle"
        process_start = datetime.now()
        found, w, = hog.detectMultiScale( frame, winStride=(strideX,strideY), padding=(8,8), scale=scale)
        # rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in found])
        # pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
        draw_detections(frame, found)

        if (len(found) > 0):
            rectsDrawn += len(found)
            text = "Motion Detected"
            recording_left = 10000

        cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
            (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        if (len(found) > 0 or recording_left > 0):
            if writer is None:
                writer = init_recorder(dest,resWidth,resHeight)
            else:     
                writer.write(frame) #Write the frame            
            process_end = datetime.now()
            duration = process_end - process_start
            seconds = duration.total_seconds()
            total_duration += seconds
            counter_processed += 1 # keeps track of number of frames recorded        

        counter += 1 # keeps track of number of frames processed
        end = datetime.now()
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            # print recording_left
            start_frame_time = datetime.now()

        if view:
            cv2.imshow("Security Feed", frame)
            # cv2.imshow("Frame Delta", frameDelta)
            # cv2.imshow("Thresh", thresh)
        
        key = cv2.waitKey(1) & 0xFF    

        if key == ord("q"):            
            break
    if writer is not None:
        writer.release()
    exitfunction()


def exitfunction():
    # Fetch global variables
    global counter, total_duration, counter_processed
    global resWidth,resHeight, camera, writer, rectsDrawn

    cv2.destroyAllWindows()

    end = datetime.now()
    elapsed = (end - start).total_seconds()

    f = open(dest + 'results_hog.txt', 'a')
    f.write('[REPORT] : HOG - motion detection operation ran for %i seconds\n' % elapsed)
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (resWidth,resHeight))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter, counter_processed))

    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (resWidth,resHeight)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print counter, elapsed
    print '[REPORT] : FPS : %f\n' % ((counter / elapsed))
    print '[REPORT] : Rectangles drawn : %i\n' % rectsDrawn 
    f.write('[REPORT] : Rectangles drawn : %i\n' % rectsDrawn)
    print '[DONE] : Operation completed at %s' % datetime.now()
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.close()
    if writer is not None:
        writer.release()
    os._exit(0) # exit the program

if __name__ == "__main__":
    main()