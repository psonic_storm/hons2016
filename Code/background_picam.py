# background_capture.py
# Description : downloads an image from the network camera every x seconds, detects motion and saves image if motion was detected.
# Author : Paul Sebeikin
# Date-created : 14 August 2016
# Date-modified : 16 September 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse
    from datetime import datetime
    from threading import Timer
    import numpy as np
    import imutils
    from picamera.array import PiRGBArray
    from picamera import PiCamera
    import os
    import math
    import cv2
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, elapsed, total_duration, recording_left = 0,0,0,0,0
writer, dest, start, alg = None, None, None, None
(resWidth,resHeight) = (None, None)

def init_recorder(dest, w,h, alg):
    algStr = None
    if alg == 0:
        algStr = 'MOG'
    elif alg == 1:
        algStr = 'MOG2'
    elif alg == 2:
        algStr = 'GMG' 
    res = '%ix%i' % (w,h)
    outpath = dest + 'BG_picam_' + algStr + '_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')#('I', '4', '2', '0')###
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer
def init_subtracotr(type):
    subtractor = None
    if type == 0:
        subtractor = cv2.bgsegm.createBackgroundSubtractorMOG()
    elif type == 1:
        subtractor = cv2.createBackgroundSubtractorMOG2()
    elif type == 2:
        subtractor = cv2.bgsegm.createBackgroundSubtractorGMG()
    else:
        print '[ERROR] : Invalid background subtraction algorithm entered : %s' % type
    return subtractor

def main():
    global counter, counter_processed, elapsed, total_duration, recording_left, writer, resWidth, resHeight, dest, start, alg

    # Parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-o", "--output", help="output directory")
    ap.add_argument("-d", "--duration", type=int, default=120, help="time in seconds to run experiment")
    ap.add_argument( '-r', '--resolution', type=int, nargs='*', help='resolution to stream the video in')
    ap.add_argument( '-a', '--algorithm', type=int, help='algorithm to use for background subtraction.  0 : MOG, 1 : MOG2, 2 : GMG')
    ap.add_argument( "-m", "--area_range", type=int, nargs='*', help="range of area for finding and tracking contours")
    ap.add_argument( '--heirarchy', type=bool, default=False, help='use heirarchy to filter contours')
    ap.add_argument( '--view', type=bool, default=False, help='set to true if a gui is required')

    args = vars(ap.parse_args())

    (resWidth,resHeight) = args["resolution"]  
    dest = args["output"]
    time = args["duration"]
    areaMin, areaMax = args["area_range"]    
    useHeirarchy = args["heirarchy"]
    alg = args["algorithm"]
    view = args["view"]
    
    fgbg = init_subtracotr(alg)

    print('[INFO] : Warming up the Pi camera...')
    camera = PiCamera()
    camera.resolution = (resWidth,resHeight)
    camera.framerate = 20
    rawCapture = PiRGBArray(camera, size=(resWidth,resHeight))
    sleep(2.0)
    
    print "[INFO] : PiCamera motion detector is running..."
    
    # motion detection preamble
    start = datetime.now()
    Timer(time, exitfunction).start()    
    start_frame_time = datetime.now()
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        
        frame = frame.array
        frame = imutils.rotate(frame, 180)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # greyscale the image
        # preprocessing.gaussianblur(gray)
        # preprocessing.thresholding(gray, preprocessing.thresholdingtype.adaptive)
        
        if frame is None:
            print '[ERROR] : Frame not received from the camera'
            break

        if counter < 5: # skip first two frames to allow for camera warm up            
            counter += 1
            rawCapture.truncate(0)
            continue

        process_start = datetime.now()
        text = "Idle"         

        fgmask = fgbg.apply(frame)
        (_, cnts, hierarchy) = cv2.findContours(fgmask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # draw only the largest contours  
        largest_contour = (None, None)
        for i in range(len(cnts)):        
            area = cv2.contourArea(cnts[i])
            if area < areaMin or area > areaMax: # ignore contours smaller than the minimum specified area
                continue
            elif useHeirarchy:                
                if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
                    continue
            elif largest_contour[0] is None:
                # print 'Contour = %s; area = %f' % (cnts[i], area)
                largest_contour = (cnts[i], area)
            elif area > largest_contour[1]:
                largest_contour = (cnts[i], area)
            
        if (largest_contour[0] is not None):
            # print 'Printing largest contour:'
            # print largest_contour
            (x, y, w, h,) = cv2.boundingRect(largest_contour[0])
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 9), 2)
            text = "Motion Detected"
            recording_left = 10000

        cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        if (recording_left > 0):
            # print recording_left
            if writer is None:
                writer = init_recorder(dest, resWidth, resHeight, alg)
            else:
                writer.write(frame) #Write the frame

                process_end = datetime.now()
                duration = process_end - process_start
                seconds = duration.total_seconds()
                total_duration += seconds
                counter_processed += 1 # keeps track of number of frames recorded

        counter += 1 # keeps track of number of frames processed
        end = datetime.now()
        end_frame_time = datetime.now()
        frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
        if (recording_left > 0):
            recording_left -= frame_duration
            start_frame_time = datetime.now()

        if view:
            # cv2.imshow('gray', gray)
            cv2.imshow('video_feed', frame)
            cv2.imshow('video_feed', fgmask)
        if cv2.waitKey(1) & 0xFF == ord('q'):            
            break
        rawCapture.truncate(0)

    if writer is not None:
        writer.release()
    exitfunction()

def exitfunction():
    end = datetime.now()
    elapsed = (end - start).total_seconds()

    f = open(dest + 'results_background_subtraction.txt', 'a')
    f.write('[REPORT] : Pi Camera Background Subtraction (Algorithm: %i) - motion detection operation ran for %i seconds\n' % (alg, elapsed))
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (resWidth,resHeight))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter_processed, counter))

    print '[REPORT] : Operation ran for %i seconds' % elapsed
    print '[REPORT] : Operation ran at %ix%i resolution' % (resWidth,resHeight)
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    print counter, elapsed
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print '[REPORT] : FPS : %f' % ((counter / elapsed))
    print '[DONE] : Operation completed at %s' % datetime.now()
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.close()

    if writer is not None:
        writer.release()
    os._exit(0) # exit the program

if __name__ == '__main__':
    main()