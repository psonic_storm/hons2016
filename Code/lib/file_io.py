# io.py
# Description : various input output and file handling functions for image processing project
# Author : Paul Sebeikin
# Date-created : 9 September 2016
# Date-modified : 9 September 2016

from wand.image import Image
import ghostscript

def writefile(student_number, task_number, scores, path):
    scores = sorted(scores, key=lambda questions: questions[0])
    f = open(path, 'w')
    f.write("student_number, task, question, answers\n")
    for i in range(len(scores)):
        if len(scores[i][1]) > 1:
            f.write("%s,%s,%i,%s,ERR\n" % (student_number, task_number, scores[i][0], scores[i][1]))    
        else:
            f.write("%s,%s,%i,%s\n" % (student_number, task_number, scores[i][0], scores[i][1]))            
    f.close()
    print('[SUCCESS] CSV file saved to: %s' % path)

def convert_pdf_to_png(input_path):
    list_images = []
    print "[INFO] : Converting pdf to png..."
    all_pages = Image(filename=input_path, resolution=200)
    for i, page in enumerate(all_pages.sequence):
        with Image(page) as page_image:
            # page_image.alpha_channel = False
            page_image.format = "png"
            output_path = './test/page-%s.png' % chr(i+97)
            page_image.save(filename=output_path)
            list_images.append(output_path)
    print "[INFO] Success.  Converted %i images" % len(list_images)
    return list_images