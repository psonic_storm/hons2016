# Test Results

##### Key
AFPT = Average Frame Processing Time
FPS = Frames Per Second
MSP = Motion Slow Percentage
QDSR = Quality Disk Size Ratio

## Raspberry Pi 3
### Iteration 1 - Network Camera (Still frame capture)

##### Scenario 1 - 640 x 480 resolution for 180 seconds
Runs 1-3 include motion
Runs 4-6 have no motion

MSP = (Average FPS with no motion - Average FPS with motion) / average FPS with no motion * 100
	= (9.46 - 7.45) / 9.46 * 100
	= 21 % faster with no motion

##### Run 1
AFPT = 53 milliseconds, 737 frames processed in 180 sec (Total frames = 1327)
FPS = 7.36
disk size = 13 203 993 bytes

##### Run 2
AFPT = 53 milliseconds, 624 frames processed in 180 sec (Total frames = 1387)
FPS = 7.70
disk size = 11 434 363 bytes

##### Run 3
AFPT = 53 milliseconds, 774 frames processed in 180 sec (Total frames = 1318)
FPS = 7.31
disk size = 13 687 205 bytes

##### Run 4
AFPT = 0 (No frames processed, total frames = 1700)
FPS = 9.44
disk size = 0 (No frames processed)

##### Run 5
AFPT = 0 (No frames processed, total frames = 1706)
FPS = 9.47
disk size = 0 (No frames processed)

##### Run 6
AFPT = 0 (No frames processed, total frames = 1708)
FPS = 9.48
disk size = 0 (No frames processed)

##### Scenario 2 - 320 x 240 resolution for 180 seconds
Runs 1-3 include motion
Runs 4-6 have no motion

MSP = (Average FPS with no motion - Average FPS with motion) / average FPS with no motion * 100
	= (11.06 - 10.40) / 11.06 * 100
	= 5.96 % faster with no motion

##### Run 1
AFPT = 14.9 milliseconds, 1532 frames processed in 180 sec (Total frames = 1922)
FPS = 10.67
disk size = 11 895 433 bytes

##### Run 2
AFPT = 14.89 milliseconds, 1744 frames processed in 180 sec (Total frames = 1880)
FPS = 10.22
disk size = 9 216 907 bytes

##### Run 3
AFPT = 15.7 milliseconds, 1824 frames processed in 180 sec (Total frames = 1860)
FPS = 10.33
disk size = 15 370 723 bytes

##### Run 4
AFPT = 0 (No frames processed, total frames = 1995)
FPS = 11.07
disk size = 0 (No frames processed)

##### Run 5
AFPT = 0 (No frames processed, total frames = 1988)
FPS = 11.03
disk size = 0 (No frames processed)

##### Run 6
AFPT = 0 (No frames processed, total frames = 1997)
FPS = 11.09
disk size = 0 (No frames processed)

##### Scenario 2 - 320 x 240 resolution for 180 seconds
Runs 1-3 include motion
Runs 4-6 have no motion

MSP = (Average FPS with no motion - Average FPS with motion) / average FPS with no motion * 100
	= (11.06 - 10.40) / 11.06 * 100
	= 5.96 % faster with no motion

##### Run 1
AFPT = 14.9 milliseconds, 1532 frames processed in 180 sec (Total frames = 1922)
FPS = 10.67
disk size = 11 895 433 bytes

##### Run 2
AFPT = 14.89 milliseconds, 1744 frames processed in 180 sec (Total frames = 1880)
FPS = 10.22
disk size = 9 216 907 bytes

##### Run 3
AFPT = 15.7 milliseconds, 1824 frames processed in 180 sec (Total frames = 1860)
FPS = 10.33
disk size = 15 370 723 bytes

##### Run 4
AFPT = 0 (No frames processed, total frames = 1995)
FPS = 11.07
disk size = 0 (No frames processed)

##### Run 5
AFPT = 0 (No frames processed, total frames = 1988)
FPS = 11.03
disk size = 0 (No frames processed)

##### Run 6
AFPT = 0 (No frames processed, total frames = 1997)
FPS = 11.09
disk size = 0 (No frames processed)

