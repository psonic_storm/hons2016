# surveillance.py
# Description : incorporating all of the python surveillance programs in one
# Author : Paul Sebeikin
# Date-created : 17 October 2016
# Date-modified : 17 October 2016

try:
    print "[INFO] Importing Libraries..."
    import argparse
    from dropbox.client import DropboxOAuth2FlowNoRedirect
    from dropbox.client import DropboxClient
    from datetime import datetime
    from threading import Timer
    import numpy as np
    import json
    import imutils
    import warnings
    import os
    import math
    import cv2
    from time import time, sleep
    import urllib
    import sys
    sys.path.append("./lib")
    import preprocessing
    from temp_image import TempImage
    print "[INFO] SUCCESS."
except ImportError, errorMessage:
    print "[ERROR] FAILED. \n\nError was: \"%s\" . Program exiting." % (errorMessage)
    quit()

counter, counter_processed, elapsed, total_duration, recording_left = 0,0,0,0,0
writer, conf, algorithm, start, camera = None, None, None, None, None

def exitfunction():
    end = datetime.now()
    elapsed = (end - start).total_seconds()

    if camera is not None:
    	camera.release()
	if conf["show_video"]:
		cv2.destroyAllWindows()

    f = open(conf["destination"] + 'results.txt', 'a')
    f.write('[REPORT] : Algorithm: %s - motion detection operation ran for %i seconds\n' % (algorithm, elapsed))
    f.write('[REPORT] : Operation ran at %ix%i resolution\n' % (conf["resolution"][0],conf["resolution"][1]))
    f.write('[REPORT] : %i frames recorded.  %i frames processed\n' % (counter_processed, counter))

    print '[REPORT] : Algorithm: %s - motion detection operation ran for %i seconds\n' % (algorithm, elapsed)
    print '[REPORT] : Operation ran at %ix%i resolution' % (conf["resolution"][0],conf["resolution"][1])
    print '[REPORT] : %i frames recorded.  %i frames processed' % (counter, counter_processed)
    if (counter_processed > 0):
        f.write('[REPORT] : Average frame recording time : %f seconds\n' % (total_duration / counter_processed))
        print '[REPORT] : Average frame recording time : %f seconds' % (total_duration / counter_processed)
    else:
        f.write('[REPORT] : No frames recorded.\n')
        print '[REPORT] : No frames recorded.'
    print counter, elapsed
    f.write('[REPORT] : FPS : %f\n' % ((counter / elapsed)))
    print '[REPORT] : FPS : %f' % ((counter / elapsed))
    print '[DONE] : Operation completed at %s' % datetime.now()
    f.write('[DONE] : Operation completed at %s\n\n' % datetime.now()) 
    f.close()

    if writer is not None:
        writer.release()
    print 'got here'
    os._exit(0) # exit the program

def draw_detections(img, rects, thickness = 1):
    for x,y,w,h in rects:
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0,255,0), thickness)

def image_download(url):
    path = urllib.urlopen(url)
    image = np.asarray(bytearray(path.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image

def init_recorder(dest, w,h, alg):
    res = '%ix%i' % (w,h)
    outpath = dest + alg + '_' + datetime.now().strftime("%b-%d-%Y %H%M%S") + '_' + res + ".avi"
    print '[INFO] : Writing to file path: %s at %ix%i resolution' % (outpath,w,h)
    codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')#('D', 'I', 'V', '4')#('W', 'M', 'V', '3')#('I', '4', '2', '0')###
    writer = cv2.VideoWriter(outpath, codec, 20, (w,h), 1)
    return writer

def init_subtractor(algorithm):
    subtractor = None
    if algorithm == "MOG":
    	try:
        	subtractor = cv2.bgsegm.createBackgroundSubtractorMOG()
    	except AttributeError:
    		subtractor = cv2.createBackgroundSubtractorMOG
    elif algorithm == "MOG2":
        subtractor = cv2.createBackgroundSubtractorMOG2()
    elif algorithm == "GMG":
    	try:
        	subtractor = cv2.bgsegm.createBackgroundSubtractorGMG()
    	except AttributeError:
    		subtractor = cv2.createBackgroundSubtractorGMG()
    else:
        print '[ERROR] : Invalid background subtraction algorithm entered : %s' % type
    return subtractor

def main():
    global counter, counter_processed, elapsed, total_duration, recording_left, writer, start, algorithm, conf, cameraq

    # Parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-c', "--conf", required=True, help="path to JSON configuration file")
    ap.add_argument('-a', "--algorithm", required=True, help="motion detection algorithm to use. Options: MOG, MOG2, GMG, ")
    args = vars(ap.parse_args())

    warnings.filterwarnings("ignore")
    algorithm = args["algorithm"]
    conf = json.load(open(args["conf"]))
    client, fgbg, firstFrame = None, None, None
    lastUploaded = datetime.now()
    motionCounter = 0

    if conf["use_dropbox"]:
		# connect to dropbox and start the session authorization process
		flow = DropboxOAuth2FlowNoRedirect(conf["dropbox_key"], conf["dropbox_secret"])
		print "[INFO] Authorize this application: {}".format(flow.start())
		authCode = raw_input("Enter auth code here: ").strip()
 
		# finish the authorization and grab the Dropbox client
		(accessToken, userID) = flow.finish(authCode)
		client = DropboxClient(accessToken)
		print "[SUCCESS] dropbox account linked"

    if algorithm in ["GMG", "MOG", "MOG2"]:
    	fgbg = init_subtractor(algorithm)

    elif algorithm == "HOG":
    	hog = cv2.HOGDescriptor()
    	hog.setSVMDetector( cv2.HOGDescriptor_getDefaultPeopleDetector() )

    elif algorithm == "VJ":
    	face_cascade = cv2.CascadeClassifier( './xml/haarcascade_frontalface_alt.xml' )
    	eye_cascade = cv2.CascadeClassifier( './xml/haarcascade_eye.xml' )

    if conf["stream_url"] is None and conf["camera"] is None and conf["image_url"] is None:
        print '[ERROR] : At least one streaming device must be specified.'
        os._exit(0)

    if conf["use_mode"] is 0:
        camera = cv2.VideoCapture(conf["camera"])
        print('[INFO] : Streaming from built-in camera')
    elif conf["use_mode"] is 1:
        camera = cv2.VideoCapture(conf["stream_url"])
        camera.set(3, conf["resolution"][0]) # 3 = CV_CAP_PROP_FRAME_WIDTH
    	camera.set(4, conf["resolution"][1]) # 4 = CV_CAP_PROP_FRAME_HEIGHT
        print('[INFO] : Streaming from network camera : %s' % path)
    elif conf["use_mode"] is 2:
    	frame = image_download(conf["image_url"])
        print('[INFO] : Streaming using still images : %s' % path)
    elif conf["use_mode"] is 3:
    	camera = PiCamera()
    	camera.resolution = conf["resolution"]
    	camera.framerate = conf["fps"]
    	rawCapture = PiRGBArray(camera, size=(camera.resolution))

    print('[INFO] : Warming up the camera...')    
    sleep(3.0)

    # motion detection preamble
    start = datetime.now()
    Timer(conf["duration"], exitfunction).start()    
    start_frame_time = datetime.now()
    
    print "[INFO] : Surveillance active..."
    
    if conf["use_mode"] is 3:
    	for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    		frame = frame.array
    		frame = imutils.rotate(frame, 180)
    		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # greyscale the image

    		if frame is None:
    			print '[ERROR] : Frame not received from the camera'
    			break

			if counter < 5: # skip first five frames to allow for camera warm up
				counter += 1
				rawCapture.truncate(0)
				continue

			process_start = datetime.now()
			text = "Idle"

			if algorithm == "VJ":
				faces_found = 0
				eyes_found = 0

				faces = face_cascade.detectMultiScale( gray, 1.3, 5, minSize=(30,30))
				for ( x, y, w, h ) in faces:
					cv2.rectangle( frame, (x, y), (x+w, y+h), (255, 0, 0), 2 )
					faces_found += 1
					roi_gray = gray[ y:y+h, x:x+w ]
					roi_color = frame[ y:y+h, x:x+w ]
					eyes = eye_cascade.detectMultiScale( roi_gray )

					for ( ex, ey, ew, eh ) in eyes:
						cv2.rectangle( roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
						eyes_found += 1

					if (faces_found > 0 or eyes_found > 0):
						text = "Motion Detected"
						recording_left = 10000

			elif algorithm == "HOG":
				found, w, = hog.detectMultiScale( frame, winStride=conf["winstride"], padding=(8,8), scale=conf["scale"])
				draw_detections(frame, found)
				if (len(found) > 0):
					rectsDrawn += len(found)
					text = "Motion Detected"
					recording_left = 10000
			
			else:
				if algorithm == "FD":
					preprocessing.gaussianblur(gray)

					if firstFrame is None:
						firstFrame = gray
						continue

					frameDelta = cv2.absdiff(firstFrame, gray)
					res = cv2.threshold(frameDelta, conf["threshold"], 255, cv2.THRESH_BINARY)[1] # threshold(src, threshold, maxVal, )
					res = cv2.dilate(res, None, iterations=2)

				elif algorithm in ["GMG", "MOG", "MOG2"]:
					res = fgbg.apply(frame)
				
				if conf["use_heirarchy"]:
					(_, cnts, hierarchy) = cv2.findContours(res.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
				else:
					(_, cnts, hierarchy) = cv2.findContours(res.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			
				# draw only the largest contours  
				largest_contour = (None, None)
				for i in range(len(cnts)):        
					area = cv2.contourArea(cnts[i])
					if area < areaMin or area > areaMax: # ignore contours smaller than the minimum specified area
						continue
					elif useHeirarchy:                
						if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
							continue
					elif largest_contour[0] is None:
						largest_contour = (cnts[i], area)
					elif area > largest_contour[1]:
						largest_contour = (cnts[i], area)

				if (largest_contour[0] is not None):
					(x, y, w, h,) = cv2.boundingRect(largest_contour[0])
					cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 9), 2)
					text = "Motion Detected"
					recording_left = 10000

			if (recording_left > 0 or faces_found > 0 or eyes_found > 0):
				timestamp = datetime.now()
				cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
				cv2.putText(frame, timestamp.strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

				if writer is None:
					writer = init_recorder(conf["destination"], conf["resolution"][0], conf["resolution"][1], algorithm)
				else:
					writer.write(frame) #Write the frame
				
				if ((timestamp - lastUploaded).seconds >= conf["min_upload_seconds"]):
					motionCounter += 1
					if (motionCounter >= conf["min_motion_frames"]):
						if conf["use_dropbox"]:
							t = TempImage()
							cv2.imwrite(t.path, frame)

							ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
							# upload the image to Dropbox and cleanup the tempory image
							print "[UPLOAD] {}".format(ts)
							path = "{base_path}/{timestamp}.jpg".format(
								base_path=conf["dropbox_base_path"], timestamp=ts)
							client.put_file(path, open(t.path, "rb"))
							t.cleanup()
	 
						# update the last uploaded timestamp and reset the motion counter
						lastUploaded = timestamp
						motionCounter = 0
			else:
				motionCounter = 0

			process_end = datetime.now()
			duration = process_end - process_start
			seconds = duration.total_seconds()
			total_duration += seconds
			counter_processed += 1 # keeps track of number of frames recorded

			counter += 1 # keeps track of number of frames processed
			end = datetime.now()
			end_frame_time = datetime.now()
			frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
			if (recording_left > 0):
				recording_left -= frame_duration

			start_frame_time = datetime.now()
			if conf["show_video"]:
				cv2.imshow('video_feed', frame)
				if algorithm in ['GMG', 'MOG', 'MOG2', 'FD']:
					cv2.imshow('worker', res)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
			rawCapture.truncate(0)
    else:
    	while True:
			if conf["use_mode"] in [0,1]:
				grabbed, frame = camera.read()

			elif conf["use_mode"] == 2:
				frame = image_download(url)
				frame = imutils.resize(frame, width=conf["resolution"][0])        

			gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # greyscale the image

			if frame is None:
				print '[ERROR] : Frame not received from the camera'
				break

			if counter < 5: # skip first five frames to allow for camera warm up            
				counter += 1
				continue

			process_start = datetime.now()
			text = "Idle"        
			faces_found = 0
			eyes_found = 0
 

			if algorithm == "VJ":				
				faces = face_cascade.detectMultiScale( gray, 1.3, 5, minSize=(30,30))
				for ( x, y, w, h ) in faces:
					cv2.rectangle( frame, (x, y), (x+w, y+h), (255, 0, 0), 2 )
					faces_found += 1
					roi_gray = gray[ y:y+h, x:x+w ]
					roi_color = frame[ y:y+h, x:x+w ]
					eyes = eye_cascade.detectMultiScale( roi_gray )

					for ( ex, ey, ew, eh ) in eyes:
						cv2.rectangle( roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
						eyes_found += 1

				if (faces_found > 0 or eyes_found > 0):
					text = "Motion Detected"
					recording_left = 10000

			elif algorithm == "HOG":
				found, w, = hog.detectMultiScale( frame, winStride=conf["winstride"], padding=(8,8), scale=conf["scale"])
				draw_detections(frame, found)
				if (len(found) > 0):
					rectsDrawn += len(found)
					text = "Motion Detected"
					recording_left = 10000
			else:
				if algorithm == "FD":
					preprocessing.gaussianblur(gray)

					if firstFrame is None:
						firstFrame = gray
						continue

					frameDelta = cv2.absdiff(firstFrame, gray)
					res = cv2.threshold(frameDelta, conf["threshold"], 255, cv2.THRESH_BINARY)[1] # threshold(src, threshold, maxVal, )
					res = cv2.dilate(res, None, iterations=2)

				elif algorithm in ["GMG", "MOG", "MOG2"]:
					res = fgbg.apply(frame)
					
				if conf["use_heirarchy"]:
						(_, cnts, hierarchy) = cv2.findContours(res.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
				else:
					(_, cnts, hierarchy) = cv2.findContours(res.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


				# draw only the largest contours  
				largest_contour = (None, None)
				for i in range(len(cnts)):        
					area = cv2.contourArea(cnts[i])
					if area < conf["min_area"] or area > conf["max_area"]: # ignore contours smaller than the minimum specified area
						continue
					elif conf["use_heirarchy"]:                
						if hierarchy[0][i][3] == -1: # no parent hierarchy then skip because it is likely a border
							continue
					elif largest_contour[0] is None:
						largest_contour = (cnts[i], area)
					elif area > largest_contour[1]:
						largest_contour = (cnts[i], area)

				if (largest_contour[0] is not None):
					(x, y, w, h,) = cv2.boundingRect(largest_contour[0])
					cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 9), 2)
					text = "Motion Detected"
					recording_left = 10000

			if (recording_left > 0 or eyes_found > 0 or faces_found > 0):
				timestamp = datetime.now()
				cv2.putText(frame, "Status: {}".format(text), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
				cv2.putText(frame, timestamp.strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

				if writer is None:
					writer = init_recorder(conf["destination"], conf["resolution"][0], conf["resolution"][1], algorithm)
				else:
					writer.write(frame) #Write the frame

				if ((timestamp - lastUploaded).seconds >= conf["min_upload_seconds"]):
					motionCounter += 1
					if (motionCounter >= conf["min_motion_frames"]):
						if conf["use_dropbox"]:
							t = TempImage()
							cv2.imwrite(t.path, frame)

							ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
							# upload the image to Dropbox and cleanup the tempory image
							print "[UPLOAD] {}".format(ts)
							path = "{base_path}/{timestamp}.jpg".format(
								base_path=conf["dropbox_base_path"], timestamp=ts)
							client.put_file(path, open(t.path, "rb"))
							t.cleanup()
	 
						# update the last uploaded timestamp and reset the motion counter
						lastUploaded = timestamp
						motionCounter = 0
			else:
				motionCounter = 0

			process_end = datetime.now()
			duration = process_end - process_start
			seconds = duration.total_seconds()
			total_duration += seconds
			counter_processed += 1 # keeps track of number of frames recorded

			counter += 1 # keeps track of number of frames processed
			end = datetime.now()
			end_frame_time = datetime.now()
			frame_duration = (end_frame_time - start_frame_time).total_seconds() * 1000
			if (recording_left > 0):
				recording_left -= frame_duration
			start_frame_time = datetime.now()

			if conf["show_video"]:
				cv2.imshow('video_feed', frame)
				if algorithm in ['GMG', 'MOG', 'MOG2', 'FD']:
					cv2.imshow('worker', res)
			if cv2.waitKey(1) & 0xFF == ord('q'):            
				break
   
    if writer is not None:
        writer.release()
    if camera is not None:
    	camera.release()
	if conf["show_video"]:
		cv2.destroyAllWindows()
    os._exit(0) # exit the program

if __name__ == '__main__':
    main()