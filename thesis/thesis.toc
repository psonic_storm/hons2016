\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Research Statement}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Research Goals}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Research Questions}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Research Approach}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Thesis Structure}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Literature Review}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Automated Surveillance Systems}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Wireless Sensor Networks}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Internet of Things}{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}ZigBee Protocol}{9}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Micro-controllers}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Atmel Range}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}XMOS}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Arduino}{12}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Raspberry Pi}{13}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Software}{14}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Python}{14}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Raspbian Operating System}{16}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Ubuntu}{17}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}OpenCV}{17}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}SimpleCV}{17}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Motion Libraries}{18}{subsection.2.4.6}
\contentsline {section}{\numberline {2.5}Image Processing}{18}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Image Segmentation}{19}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Feature Extraction}{20}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Shape Approximation}{21}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Related Work}{21}{section.2.6}
\contentsline {section}{\numberline {2.7}Summary}{23}{section.2.7}
\contentsline {chapter}{\numberline {3}Sensor Network Design Considerations}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}High-level Design Overview}{26}{section.3.1}
\contentsline {section}{\numberline {3.2}Micro-controllers}{28}{section.3.2}
\contentsline {section}{\numberline {3.3}Network Devices}{29}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Network Switch}{29}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}IP Camera}{29}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Wi-Fi Protocols}{31}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}USB and CSI Devices}{32}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}USB Camera}{32}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Pi Camera Module}{32}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Operating Systems}{33}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Raspbian Jessie 4.4}{33}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Ubuntu MATE Desktop 16.04}{34}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Image Processing Libraries}{34}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Pillow (PIL) and imutils}{35}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}NumPy}{36}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}OpenCV}{36}{subsection.3.6.3}
\contentsline {section}{\numberline {3.7}Programming Languages}{39}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Justification for Python}{39}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Python 2.7 vs. Python 3}{40}{subsection.3.7.2}
\contentsline {section}{\numberline {3.8}Miscellaneous Software}{40}{section.3.8}
\contentsline {section}{\numberline {3.9}Summary}{41}{section.3.9}
\contentsline {chapter}{\numberline {4}Sensor Network Implementation}{42}{chapter.4}
\contentsline {section}{\numberline {4.1}Technical Programming Considerations}{43}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Language and Key Features}{43}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Compiler}{44}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Python Packages}{45}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Algorithms}{46}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Background Subtraction}{47}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Frame Differencing}{48}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Face Detection}{49}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Histogram of Oriented Gradients}{51}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Still Frame Image Capture}{53}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Network Configurations Implemented}{54}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Configuration 1 - Raspberry Pi Model 2B }{56}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Configuration 2 - Raspberry Pi 3}{56}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Configuration 3 - Notebook Benchmark}{57}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}User Interaction with Sensor Network}{58}{section.4.4}
\contentsline {section}{\numberline {4.5}Summary}{60}{section.4.5}
\contentsline {chapter}{\numberline {5}Results and Discussion}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}Performance Indicators}{61}{section.5.1}
\contentsline {section}{\numberline {5.2}Experimental Method}{63}{section.5.2}
\contentsline {section}{\numberline {5.3}Results of Initial Experiments}{64}{section.5.3}
\contentsline {section}{\numberline {5.4}Results of Algorithm Variations}{68}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Background Subtraction Algorithms}{69}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Absolute Frame Difference Algorithm}{73}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Viola Jones Algorithm}{75}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Histogram of Oriented Gradients Algorithm}{76}{subsection.5.4.4}
\contentsline {subsection}{\numberline {5.4.5}Motion Effects}{79}{subsection.5.4.5}
\contentsline {section}{\numberline {5.5}Summary}{80}{section.5.5}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{82}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary of Research}{82}{section.6.1}
\contentsline {section}{\numberline {6.2}Contributions of the Research}{83}{section.6.2}
\contentsline {section}{\numberline {6.3}Future Work}{85}{section.6.3}
\contentsline {chapter}{References}{86}{Item.16}
