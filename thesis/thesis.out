\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.1.1}{Research Statement}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{Research Goals}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.3}{Research Questions}{chapter.1}% 4
\BOOKMARK [1][-]{section.1.4}{Research Approach}{chapter.1}% 5
\BOOKMARK [1][-]{section.1.5}{Thesis Structure}{chapter.1}% 6
\BOOKMARK [0][-]{chapter.2}{Literature Review}{}% 7
\BOOKMARK [1][-]{section.2.1}{Automated Surveillance Systems}{chapter.2}% 8
\BOOKMARK [1][-]{section.2.2}{Wireless Sensor Networks}{chapter.2}% 9
\BOOKMARK [2][-]{subsection.2.2.1}{Internet of Things}{section.2.2}% 10
\BOOKMARK [2][-]{subsection.2.2.2}{ZigBee Protocol}{section.2.2}% 11
\BOOKMARK [1][-]{section.2.3}{Micro-controllers}{chapter.2}% 12
\BOOKMARK [2][-]{subsection.2.3.1}{Atmel Range}{section.2.3}% 13
\BOOKMARK [2][-]{subsection.2.3.2}{XMOS}{section.2.3}% 14
\BOOKMARK [2][-]{subsection.2.3.3}{Arduino}{section.2.3}% 15
\BOOKMARK [2][-]{subsection.2.3.4}{Raspberry Pi}{section.2.3}% 16
\BOOKMARK [1][-]{section.2.4}{Software}{chapter.2}% 17
\BOOKMARK [2][-]{subsection.2.4.1}{Python}{section.2.4}% 18
\BOOKMARK [2][-]{subsection.2.4.2}{Raspbian Operating System}{section.2.4}% 19
\BOOKMARK [2][-]{subsection.2.4.3}{Ubuntu}{section.2.4}% 20
\BOOKMARK [2][-]{subsection.2.4.4}{OpenCV}{section.2.4}% 21
\BOOKMARK [2][-]{subsection.2.4.5}{SimpleCV}{section.2.4}% 22
\BOOKMARK [2][-]{subsection.2.4.6}{Motion Libraries}{section.2.4}% 23
\BOOKMARK [1][-]{section.2.5}{Image Processing}{chapter.2}% 24
\BOOKMARK [2][-]{subsection.2.5.1}{Image Segmentation}{section.2.5}% 25
\BOOKMARK [2][-]{subsection.2.5.2}{Feature Extraction}{section.2.5}% 26
\BOOKMARK [2][-]{subsection.2.5.3}{Shape Approximation}{section.2.5}% 27
\BOOKMARK [1][-]{section.2.6}{Related Work}{chapter.2}% 28
\BOOKMARK [1][-]{section.2.7}{Summary}{chapter.2}% 29
\BOOKMARK [0][-]{chapter.3}{Sensor Network Design Considerations}{}% 30
\BOOKMARK [1][-]{section.3.1}{High-level Design Overview}{chapter.3}% 31
\BOOKMARK [1][-]{section.3.2}{Micro-controllers}{chapter.3}% 32
\BOOKMARK [1][-]{section.3.3}{Network Devices}{chapter.3}% 33
\BOOKMARK [2][-]{subsection.3.3.1}{Network Switch}{section.3.3}% 34
\BOOKMARK [2][-]{subsection.3.3.2}{IP Camera}{section.3.3}% 35
\BOOKMARK [2][-]{subsection.3.3.3}{Wi-Fi Protocols}{section.3.3}% 36
\BOOKMARK [1][-]{section.3.4}{USB and CSI Devices}{chapter.3}% 37
\BOOKMARK [2][-]{subsection.3.4.1}{USB Camera}{section.3.4}% 38
\BOOKMARK [2][-]{subsection.3.4.2}{Pi Camera Module}{section.3.4}% 39
\BOOKMARK [1][-]{section.3.5}{Operating Systems}{chapter.3}% 40
\BOOKMARK [2][-]{subsection.3.5.1}{Raspbian Jessie 4.4}{section.3.5}% 41
\BOOKMARK [2][-]{subsection.3.5.2}{Ubuntu MATE Desktop 16.04}{section.3.5}% 42
\BOOKMARK [1][-]{section.3.6}{Image Processing Libraries}{chapter.3}% 43
\BOOKMARK [2][-]{subsection.3.6.1}{Pillow \(PIL\) and imutils}{section.3.6}% 44
\BOOKMARK [2][-]{subsection.3.6.2}{NumPy}{section.3.6}% 45
\BOOKMARK [2][-]{subsection.3.6.3}{OpenCV}{section.3.6}% 46
\BOOKMARK [1][-]{section.3.7}{Programming Languages}{chapter.3}% 47
\BOOKMARK [2][-]{subsection.3.7.1}{Justification for Python}{section.3.7}% 48
\BOOKMARK [2][-]{subsection.3.7.2}{Python 2.7 vs. Python 3}{section.3.7}% 49
\BOOKMARK [1][-]{section.3.8}{Miscellaneous Software}{chapter.3}% 50
\BOOKMARK [1][-]{section.3.9}{Summary}{chapter.3}% 51
\BOOKMARK [0][-]{chapter.4}{Sensor Network Implementation}{}% 52
\BOOKMARK [1][-]{section.4.1}{Technical Programming Considerations}{chapter.4}% 53
\BOOKMARK [2][-]{subsection.4.1.1}{Language and Key Features}{section.4.1}% 54
\BOOKMARK [2][-]{subsection.4.1.2}{Compiler}{section.4.1}% 55
\BOOKMARK [2][-]{subsection.4.1.3}{Python Packages}{section.4.1}% 56
\BOOKMARK [1][-]{section.4.2}{Algorithms}{chapter.4}% 57
\BOOKMARK [2][-]{subsection.4.2.1}{Background Subtraction}{section.4.2}% 58
\BOOKMARK [2][-]{subsection.4.2.2}{Frame Differencing}{section.4.2}% 59
\BOOKMARK [2][-]{subsection.4.2.3}{Face Detection}{section.4.2}% 60
\BOOKMARK [2][-]{subsection.4.2.4}{Histogram of Oriented Gradients}{section.4.2}% 61
\BOOKMARK [2][-]{subsection.4.2.5}{Still Frame Image Capture}{section.4.2}% 62
\BOOKMARK [1][-]{section.4.3}{Network Configurations Implemented}{chapter.4}% 63
\BOOKMARK [2][-]{subsection.4.3.1}{Configuration 1 - Raspberry Pi Model 2B }{section.4.3}% 64
\BOOKMARK [2][-]{subsection.4.3.2}{Configuration 2 - Raspberry Pi 3}{section.4.3}% 65
\BOOKMARK [2][-]{subsection.4.3.3}{Configuration 3 - Notebook Benchmark}{section.4.3}% 66
\BOOKMARK [1][-]{section.4.4}{User Interaction with Sensor Network}{chapter.4}% 67
\BOOKMARK [1][-]{section.4.5}{Summary}{chapter.4}% 68
\BOOKMARK [0][-]{chapter.5}{Results and Discussion}{}% 69
\BOOKMARK [1][-]{section.5.1}{Performance Indicators}{chapter.5}% 70
\BOOKMARK [1][-]{section.5.2}{Experimental Method}{chapter.5}% 71
\BOOKMARK [1][-]{section.5.3}{Results of Initial Experiments}{chapter.5}% 72
\BOOKMARK [1][-]{section.5.4}{Results of Algorithm Variations}{chapter.5}% 73
\BOOKMARK [2][-]{subsection.5.4.1}{Background Subtraction Algorithms}{section.5.4}% 74
\BOOKMARK [2][-]{subsection.5.4.2}{Absolute Frame Difference Algorithm}{section.5.4}% 75
\BOOKMARK [2][-]{subsection.5.4.3}{Viola Jones Algorithm}{section.5.4}% 76
\BOOKMARK [2][-]{subsection.5.4.4}{Histogram of Oriented Gradients Algorithm}{section.5.4}% 77
\BOOKMARK [2][-]{subsection.5.4.5}{Motion Effects}{section.5.4}% 78
\BOOKMARK [1][-]{section.5.5}{Summary}{chapter.5}% 79
\BOOKMARK [0][-]{chapter.6}{Conclusion and Future Work}{}% 80
\BOOKMARK [1][-]{section.6.1}{Summary of Research}{chapter.6}% 81
\BOOKMARK [1][-]{section.6.2}{Contributions of the Research}{chapter.6}% 82
\BOOKMARK [1][-]{section.6.3}{Future Work}{chapter.6}% 83
\BOOKMARK [0][-]{Item.16}{References}{}% 84
