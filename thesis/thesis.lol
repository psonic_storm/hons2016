\contentsline {lstlisting}{\numberline {3.1}D-Link DCS930-L video stream URL}{30}{lstlisting.3.1}
\contentsline {lstlisting}{\numberline {3.2}Sample Python/OpenCV code to enable video stream from URL source}{31}{lstlisting.3.2}
\contentsline {lstlisting}{\numberline {3.3}WPA\_supplicant file configuration entry for PEAP Wi-Fi connectivity}{31}{lstlisting.3.3}
\contentsline {lstlisting}{\numberline {3.4}Scaling an image using OpenCV vs. imutils Python package}{35}{lstlisting.3.4}
\contentsline {lstlisting}{\numberline {3.5}OpenCV FOURCC codec specification}{38}{lstlisting.3.5}
\contentsline {lstlisting}{\numberline {4.1}Command line instructions to execute motion detection program}{44}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}Configuration file sample}{45}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {4.3}OpenCV background subtraction algorithms}{48}{lstlisting.4.3}
\contentsline {lstlisting}{\numberline {4.4}Simple frame differencing algorithm}{49}{lstlisting.4.4}
\contentsline {lstlisting}{\numberline {4.5}Viola Jones rapid cascading algorithm}{50}{lstlisting.4.5}
\contentsline {lstlisting}{\numberline {4.6}Histogram of gradients algorithm}{51}{lstlisting.4.6}
\contentsline {lstlisting}{\numberline {4.7}Still frame capture algorithm}{54}{lstlisting.4.7}
